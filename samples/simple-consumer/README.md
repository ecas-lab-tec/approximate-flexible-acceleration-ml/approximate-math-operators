# Simple consumer

This example illustrates how to consume the adder from the exact library.

It also illustrates a common project that consumes this repo as a library. Some remarks are the following:

* The library can be added as a wrap dependency. The wrap file is included in [approximate-math-operators.wrap](./subprojects/approximate-math-operators.wrap).
* You can find how to include it in meson by taking the [meson.build](./meson.build) as a reference.
