/*
 * Copyright 2023
 * Author: Luis G. Leon-Vega <luis.leon@ieee.org>
 */

#pragma once

#include "exponential-lut.hpp"
#include "exponential-taylor.hpp"
#include "tanh-exp.hpp"
#include "tanh-lut.hpp"
#include "tanh-taylor.hpp"

/* Wrappers */
#include "non-linear-lut.hpp"
#include "non-linear-taylor.hpp"
