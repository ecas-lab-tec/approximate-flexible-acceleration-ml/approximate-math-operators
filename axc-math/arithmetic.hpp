/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#pragma once

/* Exact version */
#include "exact/addition.hpp"
#include "exact/arithmetic.hpp"
#include "exact/division.hpp"
#include "exact/fused-multiply-add.hpp"
#include "exact/multiplication.hpp"
#include "exact/substraction.hpp"

/* Approximate version */
#include "approximate/arithmetic.hpp"
