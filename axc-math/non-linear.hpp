/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#pragma once

/* Exact version */
#include "exact/non-linear.hpp"
#include "exact/pass-thru.hpp"
#include "exact/relu.hpp"

/* Approximate version */
#include "approximate/non-linear.hpp"
