############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_top -name $::env(TOP_FUNCTION) "$::env(TOP_FUNCTION)"

# Custom inlining (disable) - you can add any required
#set_directive_inline -off "axc_add"

# It is recommended to avoid placing pragmas within the code
# for optimisation flexibility and DSE
