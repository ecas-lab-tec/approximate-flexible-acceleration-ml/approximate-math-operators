############################################################
## Copyright 2021-2022
## Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
############################################################

define WELCOME_MESSAGE
-------------------------------------------------------------------------------
-- Flexible Accelerators Library (FAL)                                       --
-- ECASLab 2021-2022                                                         --
-------------------------------------------------------------------------------
Authors:
- MHPC. Luis G. Leon Vega <lleon95@estudiantec.cr>
- Dr.-Ing. Jorge Castro Godinez <jocastro@tec.ac.cr>

Contributors:
- Fabricio Elizondo Fernandez <faelizondo@estudiantec.cr>
- David Cordero Chavarría <dacoch215@estudiantec.cr>

This project is licensed under Apache v2.

Building System Version: v$(VERSION)
endef
